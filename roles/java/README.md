# role java

Installs Java development tools.

This generally installs the `asdf` utility with the java plugin, which can then be 
used to install a variety of Java versions

### Debian / Ubuntu / RedHat / CentOS

* java_packages: List of system packages to install.
* java_versions: List of Java versions to install using asdf
* java_global_version: Java version to activate in asdf

### Amazon

* java_extras: List of Amazon Linux Extras to enable
* java_packages: List of system packages to install.
* java_versions: List of Java versions to install using asdf.
* java_global_version: Java version to activate in asdf

### MacOSX

* java_packages: List of homebrew packages to install.
* java_versions: List of Java versions to install using asdf.
* java_global_version: Java version to activate in asdf

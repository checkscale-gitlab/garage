# role oc

Installs OpenShift command line client

### All

* oc_versions: Versions of oc to install
* oc_global_version: Version of oc to globally activate

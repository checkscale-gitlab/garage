# role base

Installs base packages.

### Linux

* base_packages: List of packages to install.

### MacOSX

* base_packages: List of Homebrew packages to install.
* base_casks: List of Homebrew Casks to install.

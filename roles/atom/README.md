# role atom

Installs Atom Editor

### Debian

* atom_repo: DEB repo for atom
* atom_repo_key: DEB repo key for atom

### RedHat

* atom_repo_baseurl: YUM repo for atom
* atom_repo_gpgkey: YUM repo key for atom

### Amazon

Same as RedHat

### MacOSX

No config.  Installs with Homebrew Cask.
